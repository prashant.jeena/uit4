import React from 'react'
import '../styles/card.css';
import picture from '../kanye.jpeg';
import editIcon from '../edit.jpg';

function Card(data) {
  return (
    <div className='card'>
        <div className='image-container'>
            <div>
                <img alt='img' src={picture} className='image'/>
            </div>
            <div className='image-text'> 
                <span>Your rating</span>
                <span>Activity</span>
            </div>
            <div className='image-number'> 
                <span>53%</span>
                <span>90%</span>
            </div>
        </div>
        <div>
            <div className='title-container'>
                <div className='title-city'>
                    <div className='title'>{data.data.name}</div>
                    <div className='city'>
                        {data.data.address.city}
                        
                    </div>
                </div>
                <div className='button'>
                    <span><img className='edit' src={editIcon}/></span>
                    <span className='editText'>Edit</span>
                </div>
            </div>
            
            {/* <div className='username'>username</div> */}
            <div className='partition'></div>
            <div className='details'>

                <div className='details-col'>
                    <div className='details-item'>Email</div>
                    <div className='details-item'>City</div> 
                    <div className='details-item'>State</div>    
                    <div className='details-item'>Country</div>
                    <div className='details-item'>Phone</div> 
                </div>
                <div className='details-col1'>
                    <div>{data.data.email}</div>
                    <div>{data.data.address.street}</div>
                    <div>{data.data.address.city}</div>
                    <div>USA</div>
                    <div>{data.data.phone}</div>
                </div>

            
            </div>
        </div>

        

    </div>
  )
}

export default Card