import logo from './logo.svg';
import './App.css';
import Card from './components/card.js';
import { useState,useEffect} from 'react';
import axios from 'axios';

function App() {

  const [response,setResponse] = useState([]);

  useEffect(()=> 
  {axios.get('https://jsonplaceholder.typicode.com/users')
  .then(res=>{
        setResponse(res.data);
        // console.log(response);    
  })},[]);
   
  
  return (
    <div className="container">
     
       { response.map((user)=>{

         return(
          
          <Card data={user} />
        )})}
      
    </div>
  );
}

export default App;
